package ru.ilnarsoultanov.views

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import ru.ilnarsoultanov.BankColors


@Composable
fun BankWidgetAuthView(authClickListener: () -> Unit) {
    BankCard() {
        Column(
            modifier = Modifier
                .background(color = BankColors.bannerBackground)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "Пожалуйста, авторизуйтесь!",
                color = BankColors.bannerTextColor,
                modifier = Modifier.padding(top = 10.dp),
                fontStyle = FontStyle.Italic,
                fontSize = 18.sp
            )

            Button(
                onClick = { authClickListener() },
                modifier = Modifier.padding(all = 7.dp)
            ) {
                Text(
                    text = "Войти",
                    color = BankColors.bannerTextColor
                )
            }
        }
    }
}
