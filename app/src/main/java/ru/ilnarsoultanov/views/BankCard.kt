package ru.ilnarsoultanov.views

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.unit.dp
import ru.ilnarsoultanov.BankColors

@Composable
fun BankCard(
    shape: Shape = RoundedCornerShape(10.dp),
    backgroundColor: Color = BankColors.bannerBackground,
    modifier: Modifier = Modifier
        .padding(all = 16.dp),
    content: @Composable () -> Unit
) {
    Card(
        elevation = 4.dp,
        modifier = modifier,
        backgroundColor = backgroundColor,
        shape = shape,
        content = content
    )
}