package ru.ilnarsoultanov.views

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.HorizontalAlignmentLine
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import ru.ilnarsoultanov.BankColors
import ru.ilnarsoultanov.data.repository.BankService
import ru.ilnarsoultanov.data.repository.Services
import ru.ilnarsoultanov.getDrawableIdByResourceName
import ru.ilnarsoultanov.compose.R

@Composable
fun BankWidgetListView(data: Services, itemClickListener: (BankService) -> Unit) {
    BankCard() {
        Column(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Row(
                modifier = Modifier.fillMaxWidth()
                    .padding(all = 12.dp)
            ) {
                Image(
                    painter = painterResource(R.drawable.ic_pb_card),
                    contentDescription = "Balance"
                )
                Spacer(modifier = Modifier.width(12.dp))
                Column() {
                    Text(
                        text = "${data.balance} ₽",
                        color = BankColors.bannerTextColor,
                        fontWeight = FontWeight.W500,
                        fontSize = 16.sp
                    )
                    Text(
                        text = "Доступный остаток",
                        color = BankColors.bannerTextColor,
                        fontWeight = FontWeight.W400,
                        fontSize = 12.sp
                    )
                }
                Image(
                    painter = painterResource(R.drawable.ic_to_left),
                    contentDescription = "Collapse banner"
                )
            }

            Spacer(modifier = Modifier.height(20.dp))

            BankCard(
                modifier = Modifier.fillMaxWidth().defaultMinSize(minHeight = 60.dp),
                backgroundColor = BankColors.bannerInnerColor
            ) {
                LazyRow(
                    modifier = Modifier.wrapContentSize()
                        .align(Alignment.CenterHorizontally)
                        .padding(start = 16.dp, end = 16.dp, top = 10.dp, bottom = 10.dp)
                ) {
                    data.services.forEach { service ->
                        item {
                            Column(
                                modifier = Modifier.clickable { itemClickListener(service) }
                                    .fillMaxWidth()
                                    .padding(start = 15.dp, end = 15.dp, top = 5.dp, bottom = 5.dp)
                            ) {
                                val resId =
                                    LocalContext.current.getDrawableIdByResourceName(
                                        drawableResourceName = service.iconName
                                    )
                                        ?: R.drawable.ic_unknown
                                Image(
                                    painter = painterResource(resId),
                                    contentDescription = service.title,
                                    modifier = Modifier
                                        .align(Alignment.CenterHorizontally)
                                )
                                Spacer(modifier = Modifier.height(4.dp))
                                Text(
                                    text = service.title,
                                    color = BankColors.bannerTextColor,
                                    modifier = Modifier.padding(top = 10.dp),
                                    fontWeight = FontWeight.W400,
                                    fontSize = 10.sp
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}





