package ru.ilnarsoultanov.views

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import ru.ilnarsoultanov.BankColors


@Composable
fun LoadingView() {
    BankCard() {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(color = BankColors.bannerBackground)
                .padding(all = 12.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "Загрузка...",
                color = BankColors.bannerTextColor,
                modifier = Modifier.padding(all = 10.dp),
                fontStyle = FontStyle.Italic,
                fontSize = 18.sp
            )
            CircularProgressIndicator(color = Color.White)
        }
    }
}