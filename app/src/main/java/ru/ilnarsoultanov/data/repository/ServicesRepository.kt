package ru.ilnarsoultanov.data.repository

class ServicesRepository {

    fun getServices(): Services {
        val list = mutableListOf<BankService>()
        list.add(
            BankService(
                title = "Платежи",
                iconName = "ic_payments"
            )
        )
        list.add(
            BankService(
                title = "Шаблоны",
                iconName = "ic_templates"
            )
        )
        list.add(
            BankService(
                title = "История",
                iconName = "ic_timeline"
            )
        )
        return Services(
            balance = 300F,
            services = list
        )
    }
}

data class Services(
    val balance: Float,
    val services: List<BankService>
)

data class BankService(
    val title: String,
    val iconName: String,
    val url: String = "https://pochtabank.ru"
)