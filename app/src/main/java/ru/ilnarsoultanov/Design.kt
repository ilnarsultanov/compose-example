package ru.ilnarsoultanov

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

object BankColors {
    val bannerBackground = Color(0xFF0055A6)
    val bannerInnerColor = Color(0xFF0F5FAB)
    val bannerTextColor = Color(0xFFFFFFFF)
}

object BankPaddings {
    val bannerPadding = 16.dp
}
