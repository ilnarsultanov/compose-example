package ru.ilnarsoultanov.compose

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.ilnarsoultanov.data.repository.Services
import ru.ilnarsoultanov.data.repository.ServicesRepository
import ru.ilnarsoultanov.views.BankWidgetAuthView
import ru.ilnarsoultanov.views.BankWidgetListView
import ru.ilnarsoultanov.views.LoadingView

class MyComposeActivity : AppCompatActivity() {
    private val servicesRepository: ServicesRepository by lazy {
        ServicesRepository()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            MaterialTheme {
                BankWidget(servicesRepository.getServices())
            }
        }
    }
}

@Composable
fun BankWidget(data: Services) {
    //типа статус, 0 - неавторизован, 1 - загрузка, 2 - есть данные
    val authState = remember { mutableStateOf(0) }
    val coroutineScope = rememberCoroutineScope()

    when (authState.value) {
        0 -> {
            BankWidgetAuthView {
                coroutineScope.launch {
                    authState.value = 1
                    delay(3000)
                    authState.value = 2
                }
            }
        }
        1 -> { LoadingView() }
        2 -> {
            BankWidgetListView(data = data) {
                //Toast.makeText(LocalContext.current, it.title, Toast.LENGTH_SHORT).show()
            }
        }
    }
}
