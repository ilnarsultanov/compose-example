package ru.ilnarsoultanov

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat


/**
 * @param drawableResourceName Название drawable ресурса
 * @return Если ресурс найден, возвращается идентификатор ресурса, иначе null
 */
@DrawableRes
fun Context.getDrawableIdByResourceName(drawableResourceName: String): Int? {
    return resources.getIdentifier(drawableResourceName, "drawable", packageName).takeIf { it != 0 }
}

/**
 * @param drawableResourceName Название drawable ресурса
 * @return Если ресурс найден, возвращается Drawable, иначе null
 */
fun Context.getDrawableByResourceName(drawableResourceName: String): Drawable? {
    return getDrawableIdByResourceName(drawableResourceName = drawableResourceName)?.let { resourceId ->
        ContextCompat.getDrawable(this, resourceId)
    }
}

/**
 * @param id Идентификатор ресурса
 * @return Если ресурс найден, возвращается Drawable, иначе null
 */
fun Context.getDrawableCompat(@DrawableRes id: Int): Drawable? {
    return ContextCompat.getDrawable(this, id)
}